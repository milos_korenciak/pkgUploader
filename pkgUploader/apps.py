from django.apps import AppConfig


class PkuploaderConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pkUploader'
